    import React, { createContext, useState, useEffect } from 'react';
    import { auth } from '../../../firebase';


    const AuthContext = createContext<any>({
    user: null,
    setUser: () => {}
    });

    interface Props {
    children: React.ReactNode;
    }

    const AuthProvider = (props: Props) => {
    const [user, setUser] = useState<null | any>(null);
    const [firebaseUser, setFirebaseUser] = useState<any>(null); 

    useEffect(() => {
        const unsubscribe = auth.onAuthStateChanged((firebaseUser:any) => {
        setFirebaseUser(firebaseUser); 

        if (firebaseUser) {
            setUser({
            uid: firebaseUser.uid,
            email: firebaseUser.email,
            });
        } else {
            setUser(null); 
        }
        });

        return () => unsubscribe(); 
    }, []);

    return (
        <AuthContext.Provider
        value={{
            user,
            setUser,
        }}
        >
        {props.children}
        </AuthContext.Provider>
    );
    };

    export { AuthContext, AuthProvider };
