import axios from "axios";
import { BookInformation } from "../types/entities";
import { addDoc, collection, deleteDoc, doc, getDocs, query, setDoc, where } from 'firebase/firestore';
import { db } from "../../../firebase";

export const fetchAllBooks = async (userId: string) => {
    try {
        const booksCollection = collection(db, 'booksInformation');
        const userBooksQuery = query(booksCollection, where('userId', '==', userId));
        const querySnapshot = await getDocs(userBooksQuery); 
        const books: BookInformation[] = [];
        querySnapshot.forEach((doc) => {
            const bookData = doc.data();
            const book: BookInformation = {
                id: doc.id,
                title: bookData?.title,
                image: bookData?.image,
                author:bookData?.author,
                publishedDate:bookData?.publishedDate,
                publisher:bookData?.publisher,
                description:bookData?.description,
                review:bookData.review,
                stars:bookData.stars,
                isbn:bookData.isbn,
                finished:bookData.finished
            };
            books.push(book);
        });
        console.log(books);
        return books;
    } catch (error) {
        console.error('Error fetching books:', error);
        throw error;
    }
};

export async function fetchOneBook(id:any) {
    try {
        const booksCollection = collection(db, 'booksInformation');
        const bookQuery = query(booksCollection, where('id', '==', id));
        const querySnapshot = await getDocs(bookQuery);
        if (querySnapshot.empty) {
            throw new Error('Book not found');
        }
        const doc = querySnapshot.docs[0];
        const bookData = doc.data();
        const book: BookInformation = {
            id: doc.id,
            title: bookData.title,
            image: bookData.image,
            author:bookData.author,
            review:bookData.review,
            stars:bookData.stars,
        };
        return book;
    } catch (error) {
        console.error('Error fetching book:', error);
        throw error;
    }
}

export async function postBook(bookInformation: BookInformation) {
    try {
        const booksCollection = collection(db, 'booksInformation');
        const docRef = await addDoc(booksCollection, bookInformation);
        console.log('Document written with ID: ', docRef.id);
        return docRef.id; 
    } catch (error) {
        console.error('Error posting book:', error);
        throw error;
    }
}

export async function updateBookInformation(bookInformation: BookInformation) {
    try {
        if (!bookInformation.id) {
            throw new Error('Book ID is missing');
        }
        const bookDocRef = doc(db, 'booksInformation', bookInformation.id);
        await setDoc(bookDocRef, bookInformation);
        console.log('Document updated successfully');
        return bookInformation.id; 
    } catch (error) {
        console.error('Error updating book information:', error);
        throw error;
    }
}

export async function deleteBookInformation(id:any) {
    try {
        const bookDocRef = doc(db, 'booksInformation', id);
        await deleteDoc(bookDocRef);
        console.log('Document deleted successfully');
    } catch (error) {
        console.error('Error deleting book information:', error);
        throw error;
    }
}

