import React, { useContext, useEffect, useState } from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import HomeScreen from '../screens/HomeScreen';
import BookShelfScreen from '../screens/BookShelfScreen';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import HomeStackScreen from './HomeStack';
import BookListScreen from '../screens/BookListScreen';
import { AuthContext } from '../provider/AuthProvider';
import { useIsFocused } from '@react-navigation/native';
import HomeStack from './HomeStack';

const Tab = createBottomTabNavigator();
const BottomTabNav=()=> {
    const { user } = useContext(AuthContext);   
    console.log("bottomr",user);
    console.log("helloooooo")
return (
    <Tab.Navigator
        initialRouteName="Feed"
        screenOptions={{
        tabBarActiveTintColor: '#e91e63',
    }}>
        <Tab.Screen name="Home" component={HomeScreen}
            options={{
                tabBarLabel: 'Home',
                tabBarIcon: ({ color, size }) => (
                <MaterialCommunityIcons name="home" color={color} size={size} />
                ),
            }}
        />
        <Tab.Screen name="BookShelf" component={BookListScreen} 
        options={{
        tabBarLabel: 'BookShelf',
        tabBarIcon: ({ color, size }) => (
        <MaterialCommunityIcons name="bookshelf" color={color} size={size}  />
        ),
    }}
        />
    </Tab.Navigator>
)
}

export default BottomTabNav
