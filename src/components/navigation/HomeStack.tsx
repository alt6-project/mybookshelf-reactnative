import React, { useContext } from 'react'
import { createStackNavigator } from "@react-navigation/stack";
import HomeScreen from '../screens/HomeScreen';
import BookShelfScreen from '../screens/BookShelfScreen';
import BarcordeScanScreen from '../screens/BarcordeScanScreen';
import CameraBarcordeScanScreen from '../screens/CameraBarcodeScannerScreen';
import BookRegistrationScreen from '../screens/BookRegistrationScreen';
import BookListScreen from '../screens/BookListScreen';
import CameraScreen from '../screens/CameraScreen';
import { AuthContext } from '../provider/AuthProvider';
import AuthStack from './AuthStack';
import WelcomeScreen from '../screens/WelcomeScreen';
import RegisterScreen from '../screens/RegisterScreen';
import LoginScreen from '../screens/LoginScreen';
import BottomTabNav from './BottomTabNav'; // BottomTabNavをインポート
import ImagePickerScreen from '../screens/ImagePickerScreen';
import RegisterBookStack from './RegisterBookStack';

const Stack = createStackNavigator();
const HomeStack = () => {
    const { user } = useContext(AuthContext); 
    console.log("homeStack",user);
    return (
        <Stack.Navigator>
            {user == null && (
                <>
                    <Stack.Screen name="Welcome" component={WelcomeScreen} options={{ headerShown: false }} />
                    <Stack.Screen name="Créer un compte" component={RegisterScreen} options={{ headerShown: false }} />
                    <Stack.Screen name="Login" component={LoginScreen} options={{ headerShown: false }} /> 
                </>
            )}
            {/* <Stack.Screen name="Home" component={HomeScreen} options={{ headerShown: false }} /> */}
            <Stack.Screen name="BottomTabNav" component={BottomTabNav} options={{ headerShown: false }} /> 
            <Stack.Screen name="Barcode" component={CameraBarcordeScanScreen} />
            <Stack.Screen name="Registration" component={BookRegistrationScreen} />
            <Stack.Screen name="Camera" component={CameraScreen} />
            <Stack.Screen name="BookList" component={BookListScreen} />
            <Stack.Screen name="ImagePicker" component={ImagePickerScreen}  />
            {/* <Stack.Screen name="RegisterBook" component={RegisterBookStack}/> */}
        </Stack.Navigator>
    );
}

export default HomeStack;

