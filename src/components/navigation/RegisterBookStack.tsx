import React, { useContext } from 'react'
import { createStackNavigator } from "@react-navigation/stack";
import HomeScreen from '../screens/HomeScreen';
import BookShelfScreen from '../screens/BookShelfScreen';
import BarcordeScanScreen from '../screens/BarcordeScanScreen';
import CameraBarcordeScanScreen from '../screens/CameraBarcodeScannerScreen';
import BookRegistrationScreen from '../screens/BookRegistrationScreen';
import BookListScreen from '../screens/BookListScreen';
import CameraScreen from '../screens/CameraScreen';
import { AuthContext } from '../provider/AuthProvider';
import AuthStack from './AuthStack';
import WelcomeScreen from '../screens/WelcomeScreen';
import RegisterScreen from '../screens/RegisterScreen';
import LoginScreen from '../screens/LoginScreen';
import BottomTabNav from './BottomTabNav'; // BottomTabNavをインポート
import ImagePickerScreen from '../screens/ImagePickerScreen';

const Stack = createStackNavigator();
const RegisterBookStack = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen name="Barcode" component={CameraBarcordeScanScreen} />
            <Stack.Screen name="Registration" component={BookRegistrationScreen} />
            <Stack.Screen name="Camera" component={CameraScreen} />
            <Stack.Screen name="BookList" component={BookListScreen} />
            <Stack.Screen name="ImagePicker" component={ImagePickerScreen}  />
        </Stack.Navigator>
    );
}

export default RegisterBookStack;