export interface BookInformation {
    id?:string;
    userId:string;
    isbn?:string;
    title:string;
    author?:string;
    description?:string;
    publisher?:string;
    image?:string;
    publishedDate?:string;
    review?:string;
    finished?:boolean;
    stars?:string;
    emoji?:string;
    bookShelf?:string;
}

export interface AuthInfo{
    email:string;
    password:string;
}

