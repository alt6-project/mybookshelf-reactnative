import { Camera } from 'expo-camera';
import { useEffect, useRef, useState } from 'react';
import { StyleSheet, Text, View, Button, Image } from 'react-native';
import * as MediaLibrary from 'expo-media-library';
import { storage } from '../../../firebase';
import { ref, uploadBytes, getDownloadURL } from "firebase/storage";

export default function CameraScreen({navigation,route}:any) {
    let cameraRef = useRef<any>();
    const [hasCameraPermission, setHasCameraPermission] = useState<any>();
    const [hasMediaLibraryPermission, setHasMediaLibraryPermission] = useState<any>();
    const [photo, setPhoto] = useState<any>();
    const [imageUrl,setImageUrl] = useState<any>();
    const [bookInfo,setBookInfo]=useState<any>();
    const { info } = route.params;
    useEffect(() => {
        setBookInfo(info);
        (async () => {
            const cameraPermission = await Camera.requestCameraPermissionsAsync();
            const mediaLibraryPermission = await MediaLibrary.requestPermissionsAsync();
            setHasCameraPermission(cameraPermission.status === "granted");
            setHasMediaLibraryPermission(mediaLibraryPermission.status === "granted");
        })();
    }, [])

    if (hasCameraPermission === undefined) {
        return <Text>Requesting permissions...</Text>
    } else if (!hasCameraPermission) {
        return <Text>Permission for camera not granted. Please change this in settings.</Text>
    }

    const takePic = async () => {
        const options = {
            quality: 1,
        };
        const newPhoto = await cameraRef?.current?.takePictureAsync(options);
        setPhoto(newPhoto);
    }

    const uploadImageToFirebase = async (uri: string) => {
        try {
            const response = await fetch(uri);
            const blob = await response.blob();
            const imgName = blob?.data?.name;
            const storageRef = ref(storage, `images/${imgName}`);
            await uploadBytes(storageRef, blob);
            const downloadURL = await getDownloadURL(storageRef);
            return downloadURL;
        } catch (error) {
            console.error("Error uploading image: ", error);
            throw error;
        }
    };

    const savePhoto = async () => {
        if (photo) {
            try {
                await MediaLibrary.saveToLibraryAsync(photo.uri);
                const downloadURL = await uploadImageToFirebase(photo.uri);
                setImageUrl(downloadURL);
                navigation.navigate("Registration",{info:bookInfo,imageUrl:downloadURL});
                console.log("Uploaded photo to Firebase Storage:", downloadURL);
            } catch (error) {
                console.error("Error saving photo:", error);
            } finally {
                setPhoto(undefined);
            }
        }
    };

    return (
        <View style={styles.container}>
            {photo ? (
                <Image style={styles.preview} source={{ uri: photo.uri }} />
            ) : (
                <Camera style={styles.camera} ref={cameraRef}>
                    <Button title="Take Picture" onPress={takePic} />
                </Camera>
            )}
            <View style={styles.buttonContainer}>
                <Button title="Cancel" onPress={()=>{console.log("cancel")}} disabled={!photo} />
                <Button title="Save" onPress={savePhoto} disabled={!photo || !hasMediaLibraryPermission} />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    camera: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    preview: {
        flex: 1,
        justifyContent: 'space-between',
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        marginBottom: 20,
    },
});
