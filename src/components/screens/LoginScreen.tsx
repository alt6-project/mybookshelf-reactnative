import React, { useContext, useState } from 'react';
import { signInWithEmailAndPassword } from 'firebase/auth';
import {
View,
TextInput,
Text,
TouchableOpacity,
KeyboardAvoidingView,
} from 'react-native';
import { auth } from '../../../firebase';
import { AuthContext } from '../provider/AuthProvider';

const LoginScreen = ({navigation}:any) => {
const [email, setEmail] = useState('');
const [password, setPassword] = useState('');
const [errorMessage,setErrorMessage]=useState<String>('');
const { setUser } = useContext(AuthContext); 
const handleLogin = async () => {
    try {
    const userCredential =await signInWithEmailAndPassword(auth, email, password);
    setUser(userCredential);
    navigation.navigate("BottomTabNav");
    } catch (error) {
    setErrorMessage(error.message);
    }
};

return (
    <KeyboardAvoidingView
    behavior="padding"
    style={{
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    }}
    >
    <Text style={{ fontSize: 20, marginBottom: 20 }}>Log in</Text>
    <Text style={{fontSize:16, color:"red", margin:10}}>{errorMessage!="" && errorMessage}</Text>
    <View style={{ marginBottom: 20 }}>
        <TextInput
        style={{
            width: 250,
            borderWidth: 1,
            padding: 5,
            borderColor: 'gray',
        }}
        onChangeText={setEmail}
        value={email}
        placeholder="Your email adress"
        autoCapitalize="none"
        autoCorrect={false}
        />
    </View>
    <View style={{ marginBottom: 20 }}>
        <TextInput
        style={{
            width: 250,
            borderWidth: 1,
            padding: 5,
            borderColor: 'gray',
        }}
        onChangeText={setPassword}
        value={password}
        placeholder="Your password"
        secureTextEntry={true}
        autoCapitalize="none"
        />
    </View>
    <TouchableOpacity
        style={{
        padding: 10,
        backgroundColor: '#88cb7f',
        borderRadius: 10,
        }}
        onPress={handleLogin}
        // disabled={!email || !password}
    >
        <Text style={{ color: 'white' }}>Log in</Text>
    </TouchableOpacity>
    </KeyboardAvoidingView>
);
};

export default LoginScreen;
