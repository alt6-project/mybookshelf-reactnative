import { useEffect, useRef, useState } from 'react';
import { StyleSheet, View, Button, Image, TouchableOpacity,Text } from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import { storage } from '../../../firebase';
import { ref, uploadBytes, getDownloadURL } from "firebase/storage";

export default function ImagePickerScreen({ navigation }: any) {
    let cameraRef = useRef<any>();
    const [selectedImage, setSelectedImage] = useState<any>(null);

    useEffect(() => {
        (async () => {
            const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
            if (status !== 'granted') {
                alert('Sorry, we need media library permissions to make this work!');
            }
        })();
    }, [])

    const pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1,
        });
        if (result.canceled==false) {
            console.log( result.assets[0].uri);
            setSelectedImage(result.assets[0].uri);
        }
    };

    const uploadImageToFirebase = async (uri: string) => {
        try {
            const response = await fetch(uri);
            const blob = await response.blob();
            const imgName = uri.split('/').pop();
            const storageRef = ref(storage, `images/${imgName}`);
            await uploadBytes(storageRef, blob);
            const downloadURL = await getDownloadURL(storageRef);
            console.log("File available at", downloadURL);
            return downloadURL;
        } catch (error) {
            console.error("Error uploading image: ", error);
            throw error;
        }
    };

    const savePhoto = async () => {
        if (selectedImage) {
            try {
                const downloadURL = await uploadImageToFirebase(selectedImage);
                console.log("Uploaded photo to Firebase Storage:", downloadURL);
                setSelectedImage(null);
            } catch (error) {
                console.error("Error saving photo:", error);
            }
        }
    };

    return (
        <View style={styles.container}>
            {selectedImage ? (
                <Image source={{ uri: selectedImage }} style={styles.preview} />
            ) : (
                <Button title="Pick an image from camera roll" onPress={pickImage} />
            )}
            <View style={styles.buttonContainer}>
                <Button title="Cancel" onPress={() => setSelectedImage(null)} disabled={!selectedImage} />
                <Button title="Save" onPress={savePhoto} disabled={!selectedImage} />
            </View>
            <View>
            <TouchableOpacity
            onPress={() => navigation.navigate("Barcode")}
            > 
            <Text>Annuler</Text>
            </TouchableOpacity>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    preview: {
        width: 300,
        height: 300,
        resizeMode: 'contain',
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginTop: 20,
        width: '80%',
    },
});
