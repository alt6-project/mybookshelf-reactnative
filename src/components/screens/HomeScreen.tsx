import React, { useContext } from 'react'
import { StatusBar, View,Text,TouchableOpacity } from 'react-native'
import navigation from '../navigation'
import { Entypo } from '@expo/vector-icons';
import { auth } from '../../../firebase';
import { signOut } from '@firebase/auth';
import { AuthContext } from '../provider/AuthProvider';

function HomeScreen({navigation}:any) {
    const {user,setUser}=useContext(AuthContext);
    console.log("Home",user); 
    const handleLogout = () => {
        setUser("")
        signOut(auth)
        .then(() => {
            navigation.navigate("Welcome");
            console.log('logout');
        })
        .catch((error) => {
            console.log(error.message);
        });
    };
return (
    <View className="flex-1 items-center justify-center">
        <Text className="font-extrabold">Scan barcode of your book</Text>
        <StatusBar style="auto"/>
        <TouchableOpacity
        onPress={() => navigation.navigate("Barcode")}
        > 
        <Entypo name="camera" size={24} color="black" />
        </TouchableOpacity>
        <TouchableOpacity
        onPress={handleLogout}
        style={{
        marginTop: 10,
        padding: 10,
        backgroundColor: '#88cb7f',
        borderRadius: 10,
        width: 100,
        }}
    >
        <Text style={{ color: 'white' }}>{`Log out`}</Text>
    </TouchableOpacity>
    </View>
)
}

export default HomeScreen
