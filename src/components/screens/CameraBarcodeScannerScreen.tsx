import React, { useState, useEffect } from "react";
import { Text, StyleSheet, Button, SafeAreaView, Image, View, Pressable} from "react-native";
import { CameraView, Camera } from "expo-camera/next";
import axios from "axios";
import { useNavigation } from '@react-navigation/native';
import ModalRegistration from "../components/ModalRegistration";

export default function CameraBarcodeScannerScreen() {
    const [hasPermission, setHasPermission] = useState<any | null>(null);
    const [scanned, setScanned] = useState(true);
    const [bookInfo, setBookInfo] = useState(null);
    const navigation = useNavigation<any>();
    const [modalVisible, setModalVisible] = useState(false);

    useEffect(() => {
        (async () => {
            const { status } = await Camera.requestCameraPermissionsAsync();
            setHasPermission(status === "granted");
        })();
    }, []);

    const handleBarCodeScanned = ({ type, data }: any) => {
        setScanned(true);
        const URL = 'https://www.googleapis.com/books/v1/volumes?q=isbn:' + data;
        alert(`Bar code with type ${type} and data ${data} has been scanned! ${URL}`);
        fetchBooks(URL);
        console.log(URL);
    };

    const fetchBooks = async (URL: any) => {
        try {
            const response = await axios.get(URL);
            //mettre un information d'un livre
            setBookInfo(response.data.items[0]);
        } catch (error) {
            alert("error occurred");
        }
    };

    if (hasPermission === null) {
        return <Text>Requesting for camera permission</Text>;
    }
    if (hasPermission === false) {
        return <Text>No access to camera</Text>;
    }

    const styles = StyleSheet.create({
        barcode: {
            width: "100%",
            height: "40%"
        },
        container: {
            flex: 1
        }
    });
    const resetSession = () => {
        setScanned(false);
        setBookInfo(null);
    };
    return (
        <SafeAreaView style={styles.container}>
            <CameraView
                onBarcodeScanned={scanned ? undefined : handleBarCodeScanned}
                barcodeScannerSettings={{
                    barcodeTypes: ["ean13","ean8"],
                }}
                style={styles.barcode}
            ></CameraView>
            {scanned && (
                <Button title={"Tap to Scan Again"} onPress={() => resetSession()} />
            )}
            {bookInfo !== null ? (
                <>
                    <Text>Le title de livre {bookInfo.volumeInfo.title} un jour de publication {bookInfo?.volumeInfo?.publishedDate}</Text>
                    {bookInfo?.volumeInfo?.imageLinks ?
                        (<Image source={{ uri: bookInfo.volumeInfo.imageLinks.thumbnail }} style={{ width: 100, height: 150 }} />) :
                        <View style={{ width: 100, height: 150, backgroundColor: "lightgreen" }}>
                            <Text>No Image found</Text>
                        </View>
                    }
                <Button title="registration" onPress={()=>{navigation.navigate("Registration",{info:bookInfo})
            setBookInfo(null)}}></Button>
                    <Pressable
                        style={[styles.button, styles.buttonClose]}
                        onPress={() => setModalVisible(!modalVisible)}>
                        <Text style={styles.textStyle}>Hide Modal</Text>
                    </Pressable>
                    <ModalRegistration modalVisible={modalVisible} setModalVisible={setModalVisible}/>
                </>
            ) : null}
        </SafeAreaView>
    );
}


const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 22,
        },
        modalView: {
        margin: 20,
        backgroundColor: 'white',
        borderRadius: 20,
        padding: 35,
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
        },
        button: {
        borderRadius: 20,
        padding: 10,
        elevation: 2,
        },
        buttonOpen: {
        backgroundColor: '#F194FF',
        },
        buttonClose: {
        backgroundColor: '#2196F3',
        },
        textStyle: {
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center',
        },
        modalText: {
        marginBottom: 15,
        textAlign: 'center',
        },
    });
