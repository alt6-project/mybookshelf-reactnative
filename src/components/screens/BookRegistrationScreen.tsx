import React, { useState, useEffect, useCallback, useContext } from "react";
import { Button, StyleSheet, View, ScrollView, Text, Image, TextInput, Switch, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useForm, Controller } from "react-hook-form";
import { Picker } from '@react-native-picker/picker';
import { Entypo } from '@expo/vector-icons';
import { db } from "../../../firebase";
import { getFirestore, collection, addDoc } from 'firebase/firestore';
import { AuthContext } from "../provider/AuthProvider";

export default function BookRegistrationScreen({ route }: any) {
    const {user}=useContext(AuthContext); 
    const navigation = useNavigation<any>();
    const { info,imageUrl} = route.params;
    const [bookInfo, setBookInfo] = useState(null);
    const [imageLink,setImageLink]=useState(null);
    useEffect(() => {
        setBookInfo(info);
        if(info.volumeInfo.imageLinks){
            setImageLink(info?.volumeInfo?.imageLinks?.thumbnail);
        }
        if(imageUrl!=null){
            setImageLink(imageUrl);
        }
    }, [imageUrl])
    const { control, handleSubmit, setValue, formState: { errors } } = useForm({
        defaultValues: {
            review: '',
            finished:false,
            stars: " "
        } 
    });
    const onSubmit = async(data: any) => {
        navigation.navigate("BookList");
        setBookInfo({ ...data, title: bookInfo?.volumeInfo?.title, author: bookInfo?.volumeInfo.authors[0],isbn:bookInfo?.volumeInfo?.industryIdentifiers[1]?.identifier,publishedData:bookInfo?.volumeInfo?.publishedDate})
        addDoc(collection(db, 'booksInformation'), {
            userId:user?.user?.uid,
            isbn:bookInfo.volumeInfo.industryIdentifiers[1].identifier,
            title: bookInfo?.volumeInfo?.title,
            author: bookInfo?.volumeInfo?.authors[0],
            image:imageLink,
            review: data.review,
            finished: data.finished,
            stars: data.stars
        })
        .then((docRef) => {
            setBookInfo(null);
            setImageLink(null);
            console.log('bookReview added:', docRef.id);
        })
        .catch((error) => {
            console.error('error uploading:', error);
        });
    }
    return (
        <ScrollView style={styles.container}>
            {bookInfo &&
                <>
                    <Text>Le title de livre{bookInfo?.volumeInfo?.title} un jour de publication {bookInfo?.volumeInfo?.publishedDate}{bookInfo?.volumeInfo?.authors[0]}</Text>
                    {imageLink?
                        (<Image source={{ uri: imageLink }} style={{ width: 100, height: 100 }} />):   (<View style={{ width: 100, height: 150, backgroundColor: "lightgreen" }}>
                        <Text>No Image found</Text>
                        <TouchableOpacity
                        onPress={() => navigation.navigate("Camera",{info:bookInfo})}
                        > 
                        <Entypo name="camera" size={24} color="black" />
                        <Text> take photo</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                        onPress={() => navigation.navigate("ImagePicker")}
                        > 
                        <Text>select from image picker</Text>
                        </TouchableOpacity>
                    </View>)
                    }
                </>
            }
            <Text>Your opinion</Text>
            <Controller
                control={control}
                render={({ field: { onChange, onBlur, value } }) => (
                    <TextInput
                        placeholder="Write Your opinion"
                        onBlur={onBlur}
                        onChangeText={onChange}
                        value={value}
                    />
                )}
                name="review"
            />
            {errors.review && <Text>This is required.</Text>}
            <Text>Read</Text>
            <Controller
                control={control}
                render={({ field: { onChange, value } }) => (
                    <Switch
                        onValueChange={onChange}
                        value={value}
                    />
                )}
                name="finished"
            />
            <Controller
                control={control}
                render={({ value }: any) => (
                    <View style={styles.picker}>
                        <Picker
                            selectedValue={value}
                            onValueChange={itemValue => setValue("stars", itemValue)}
                        >
                            <Picker.Item label="⭐️" value="1" />
                            <Picker.Item label="⭐️⭐️" value="2" />
                            <Picker.Item label="⭐️⭐️⭐️" value="3" />
                            <Picker.Item label="⭐️⭐️⭐️⭐️" value="4" />
                            <Picker.Item label="⭐️⭐️⭐️⭐️⭐️" value="5" />
                        </Picker>
                    </View>
                )}
                name="stars"
                defaultValue="3"
            />
            <Button title="Submit" onPress={handleSubmit(onSubmit)} />
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    container:{},
    picker: {
        backgroundColor: '#f2f2f2',
        marginBottom: 10,
        borderRadius: 6,
    },
})

