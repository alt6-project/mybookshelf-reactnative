import React, { useContext, useState } from 'react';
import { createUserWithEmailAndPassword } from 'firebase/auth';
import {
    View,
    TextInput,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    } from 'react-native';
import { auth } from '../../../firebase';
import { AuthContext } from '../provider/AuthProvider';

export default function RegisterScreen (){
    const { setUser } = useContext(AuthContext); 
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [errorMessage,setErrorMessage]=useState<String>('');
    const handleRegister = async () => {
        try {
        const user = await createUserWithEmailAndPassword(auth, email, password);
        setUser(user)
        } catch (error) {
        setErrorMessage(error?.message)
        console.log(error);
        }
    };
    return (
        <KeyboardAvoidingView
        behavior="padding"
        style={{
            justifyContent: 'center',
            alignItems: 'center',
            flex: 1,
        }}
        >
        <Text style={{ fontSize: 20, marginBottom: 20 }}>Créer un compte</Text>
        <Text style={{fontSize:16, color:"red", margin:10}}>{errorMessage!="" && errorMessage}</Text>
        <View style={{ marginBottom: 20 }}>
            <TextInput
            style={{
                width: 250,
                borderWidth: 1,
                padding: 5,
                borderColor: 'gray',
            }}
            onChangeText={setEmail}
            value={email}
            placeholder="Your email adress"
            autoCapitalize="none"
            autoCorrect={false}
            />
        </View>
        <View style={{ marginBottom: 20 }}>
            <TextInput
            style={{
                width: 250,
                borderWidth: 1,
                padding: 5,
                borderColor: 'gray',
            }}
            onChangeText={setPassword}
            value={password}
            placeholder="Your password"
            secureTextEntry={true}
            autoCapitalize="none"
            />
        </View>
        <TouchableOpacity
            style={{
            padding: 10,
            backgroundColor: '#88cb7f',
            borderRadius: 10,
            }}
            onPress={handleRegister}
        >
            <Text style={{ color: 'white' }}>Register</Text>
        </TouchableOpacity>
        </KeyboardAvoidingView>
    );
};