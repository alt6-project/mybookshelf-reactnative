import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'

function WelcomeScreen({navigation}:any) {
return (
    <View className="flex-1 items-center justify-center">
    <Text className="font-extrabold">Welcome</Text>
    
    <TouchableOpacity
        onPress={()=>navigation.navigate("Créer un compte")}
        style={{
        marginTop: 10,
        padding: 10,
        backgroundColor: '#88cb7f',
        borderRadius: 10,
        width: 200,
        }}
        >
        <Text style={{ color: 'white' }}>{`créer un compte`}</Text>
    </TouchableOpacity>

    <TouchableOpacity
        onPress={()=>navigation.navigate("Login")}
        style={{
        marginTop: 10,
        padding: 10,
        backgroundColor: '#88cb7f',
        borderRadius: 10,
        width: 200,
        }}
        >
        <Text style={{ color: 'white' }}>{`Log in`}</Text>
    </TouchableOpacity>
</View>
)
}

export default WelcomeScreen
