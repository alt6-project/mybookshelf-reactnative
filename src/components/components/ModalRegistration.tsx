
import React, { useState} from "react";
import { Button, StyleSheet, View,Text, Image, TextInput, Switch, TouchableOpacity,Modal } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useForm, Controller } from "react-hook-form";
import { postBook } from "../services/bookshelfService";
import { Entypo } from '@expo/vector-icons';
import StarRating from "react-native-star-rating-widget";
function ModalRegistration({modalVisible, setModalVisible, bookInfo}:any) {
    const navigation = useNavigation<any>();
    const [info,setInfo]=useState(bookInfo);
    const { control, handleSubmit, setValue, formState: { errors } } = useForm({
        defaultValues: {
            review: '',
            finished:false,
            stars: " "
        } 
    });
    const onSubmit = async(data: any) => {
        setModalVisible(!modalVisible);
        setInfo({ ...data, title: bookInfo.volumeInfo.title, author: bookInfo.volumeInfo.authors[0],isbn:bookInfo.volumeInfo.industryIdentifiers[1].identifier,image:bookInfo.volumeInfo.imageLinks.thumbnail,publishedData:bookInfo.volumeInfo.publishedDate})
        fo.industryIdentifiers[1].identifier,image:bookInfo.volumeInfo.imageLinks.thumbnail,publishedData:bookInfo.volumeInfo.publishedDconsole.log(bookInfo)
        console.log({ ...data, title: bookInfo.volumeInfo.title, author: bookInfo.volumeInfo.authors[0],isbn:bookInfo.volumeInate});
    }
return (
    <Modal
    animationType="slide"
    transparent={true}
    visible={modalVisible}
    onRequestClose={() => {
        setModalVisible(!modalVisible);
        }}>
        <View style={styles.modalView}>
            {bookInfo &&
                <>
                    <Text>Le title de livre{bookInfo.volumeInfo.title} un jour de publication {bookInfo.volumeInfo.publishedDate}{bookInfo.volumeInfo.authors[0]}</Text>
                    {bookInfo.volumeInfo.imageLinks?
                        (<Image source={{ uri: bookInfo.volumeInfo.imageLinks.thumbnail }} style={{ width: 100, height: 100 }} />):   (<View style={{ width: 100, height: 150, backgroundColor: "lightgreen" }}>
                        <Text>No Image found</Text>
                        <TouchableOpacity
                        onPress={() => navigation.navigate("Camera")}
                        > 
                        <Entypo name="camera" size={24} color="black" />
                        </TouchableOpacity>
                    </View>)
                    }
                </>
            }
            <Text>Your opinion</Text>
            <Controller
                control={control}
                render={({ field: { onChange, onBlur, value } }) => (
                    <TextInput
                        placeholder="Write Your opinion"
                        onBlur={onBlur}
                        onChangeText={onChange}
                        value={value}
                    />
                )}
                name="review"
            />
            {errors.review && <Text>This is required.</Text>}
            <Text>Read</Text>
            <Controller
                control={control}
                render={({ field: { onChange, value } }) => (
                    <Switch
                        onValueChange={onChange}
                        value={value}
                    />
                )}
                name="finished"
            />
        <Controller
                control={control}
                render={({ field: { onChange, value }}) => (
                    <StarRating
                    rating={value}
                    onChange={onChange}
                />
                )}
                name="stars"
            />  
            <Button title="Submit" onPress={handleSubmit(onSubmit)} />
        </View>
    </Modal>
)
}

export default ModalRegistration


const styles = StyleSheet.create({
    container:{backgroundColor:"#ffffff"},
    picker: {
        backgroundColor: '#f2f2f2',
        marginBottom: 10,
        borderRadius: 6,
    },
    centeredView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 22,
        },
        modalView: {
        color:"black",
        margin: 20,
        backgroundColor: 'white',
        borderRadius: 20,
        padding: 35,
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
        },
        button: {
        borderRadius: 20,
        padding: 10,
        elevation: 2,
        },
        buttonOpen: {
        backgroundColor: '#F194FF',
        },
        buttonClose: {
        backgroundColor: '#2196F3',
        },
        textStyle: {
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center',
        fontFamily:"BalsamiqSans_400Regular"
        },
        modalText: {
        marginBottom: 15,
        textAlign: 'center',   
        },
    });