import * as React from 'react';
import { TextInput,View } from 'react-native';

interface Props{
    value:string;
    setValue:any;
    placeHolder:string;
    secureTextEntry?: boolean; 
}

export const TextForm = ({value,setValue,placeHolder, secureTextEntry}:Props) =>{
    return(
        <View style={{ marginBottom: 20 }}>
            <TextInput
            style={{ width: 250, borderWidth: 2, padding: 10, borderColor: 'gray',borderRadius:10,fontFamily:"BalsamiqSans_400Regular"}}
            onChangeText={setValue}
            value={value}
            placeholder={placeHolder}
            autoCapitalize="none"
            autoCorrect={false}
            secureTextEntry={secureTextEntry} 
            />
        </View>
    );
}