import * as React from 'react';
import { Text,TouchableOpacity} from 'react-native';

interface Props{
    onPressAction:any;
    buttonText:string;
    color?:string;
}

export const Button1 = ({onPressAction,buttonText,color}:Props) =>{
    return(
    <TouchableOpacity
        style={{ padding: 10, width:150, backgroundColor: color ? color : '#88cb7f', borderRadius: 10,}}
        onPress={onPressAction}
    >
        <Text style={{ color: 'white', fontFamily:"BalsamiqSans_400Regular",textAlign:"center" }}>{buttonText}</Text>
    </TouchableOpacity>
    );
}