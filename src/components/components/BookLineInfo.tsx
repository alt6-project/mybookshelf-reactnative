import * as React from 'react';
import { Text,TouchableOpacity} from 'react-native';

interface Props{
    title:string;
    content:string
}

export const BookLineInfo = ({title,content}:Props) =>{
    return(
        <Text style={{fontFamily:"BalsamiqSans_400Regular"}}>
            <Text style={{color:"gray"}}>{title}:</Text> 
            <Text style={{fontSize:16}}>{content}</Text>
        </Text>
    );
}