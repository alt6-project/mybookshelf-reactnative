import * as React from 'react';
import { Text,TouchableOpacity} from 'react-native';

interface Props{
    onPressAction:any;
    buttonText:string;
    color?:string;
}

export const Button2 = ({onPressAction,buttonText,color}:Props) =>{
    return(
        <TouchableOpacity
        onPress={onPressAction}
        style={{
        marginTop: 10,
        padding: 10,
        backgroundColor: color?color:'#88cb7f',
        borderRadius: 10,
        width: 200,
        }}
        >
        <Text style={{ color: 'white' ,fontFamily:"BalsamiqSans_400Regular", textAlign:"center",fontSize:18}}>{buttonText}</Text>
    </TouchableOpacity>
    );
}