import { BookImage, Heading, Title } from "./Basics";
import * as React from 'react';
import { Image, SafeAreaView, ScrollView, StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { BookInformation } from "../types/entities";
import { BookLineInfo } from "./BookLineInfo";


interface props {
    bookInfo: BookInformation,
}

export const BookInfo = ({ bookInfo }: props) => {
    const [showDescription, setShowDescription] = React.useState<boolean>(false);

    const toggleDescription = () => {
        setShowDescription(!showDescription);
    };

    return (
        <View style={{ flexDirection: "column", justifyContent: "center", alignItems: "center" }}>
            <View style={{ padding: 20, minWidth: 350 }}>
                <BookLineInfo title="Title" content={bookInfo?.title} />
                <BookLineInfo title="Autor" content={bookInfo?.author} />
                <BookLineInfo title="Publisher" content={bookInfo?.publisher} />
                <BookLineInfo title="Année" content={bookInfo?.publishedDate} />                
                {showDescription ? (
                    <BookLineInfo title="Description" content={bookInfo?.description} />
                ):<BookLineInfo title="Description" content={bookInfo?.description && bookInfo?.description.length > 100 ? `${bookInfo?.description.slice(0, 100)}...` : bookInfo?.description} />}
                {bookInfo?.description !=null &&(
                <TouchableOpacity onPress={toggleDescription}>
                    <Text>{showDescription ? "Hide Description" : "Show Description"}</Text>
                </TouchableOpacity>
                )}
            </View>
            <View style={{}}>
                {bookInfo?.image ?
                    (<View>
                        <Image source={{ uri: bookInfo?.image }} style={{ width: 100, height: 150, borderRadius: 5 }} />
                    </View>) :
                    <View style={{ width: 100, height: 150, backgroundColor: "lightgreen" }}>
                        <Text>No Image found</Text>
                    </View>
                }
            </View>
        </View>
    );
};

export const bookInfoStyles = StyleSheet.create({
    card: {
        width: 300,
        height: 200,
        padding: 12,
        marginRight: 16,
        borderWidth: 1,
        borderColor: '#E7E3EB',
        borderRadius: 12,
        backgroundColor: '#FFFFF',
    },
    title: {
        textAlign: 'center',
        paddingTop: 8,
        fontFamily: "BalsamiqSans_400Regular"
    },
});
