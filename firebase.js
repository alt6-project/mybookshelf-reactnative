// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from 'firebase/firestore';
import { getStorage } from 'firebase/storage';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
import { getAuth } from 'firebase/auth';
// Your web app's Firebase configuration
export const firebaseConfig = {
    apiKey: "AIzaSyA_k2eX-rTM6NpBxdQFt-alppayS7-q-XU",
    authDomain: "mybookshelf-686eb.firebaseapp.com",
    projectId: "mybookshelf-686eb",
    storageBucket: "mybookshelf-686eb.appspot.com",
    messagingSenderId: "1061327782010",
    appId: "1:1061327782010:web:5fc0eb5430c51a98cf3bc3"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const db = getFirestore(app);
export const storage = getStorage(app);