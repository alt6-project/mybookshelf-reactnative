import React, { useContext, useEffect } from 'react';
import Navigation from './src/components/navigation';
import { SafeAreaProvider } from "react-native-safe-area-context";
import { onAuthStateChanged } from 'firebase/auth';
import { auth } from './firebase';
import { AuthContext, AuthProvider } from './src/components/provider/AuthProvider'; 
import AppLoading from 'expo-app-loading';
import {
  useFonts,
  BalsamiqSans_400Regular,
  BalsamiqSans_400Regular_Italic,
  BalsamiqSans_700Bold,
  BalsamiqSans_700Bold_Italic,
} from '@expo-google-fonts/balsamiq-sans';

export default function App() {

  const { setUser } = useContext(AuthContext); 
  useEffect(() => {
    const unsubscribe = onAuthStateChanged(auth, (user) => {
      setUser(user);
    });

    return () => unsubscribe();
  }, []);

  const [fontsLoaded] = useFonts({
    BalsamiqSans_400Regular,
    BalsamiqSans_400Regular_Italic,
    BalsamiqSans_700Bold,
    BalsamiqSans_700Bold_Italic,
  });

  if (!fontsLoaded) {
    return (
      <AppLoading />
    );
  }
  return (
    <SafeAreaProvider>
      <AuthProvider>
        <Navigation />
      </AuthProvider>
    </SafeAreaProvider>
  );
}
